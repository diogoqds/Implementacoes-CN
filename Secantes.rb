def CalculaFuncao valor, funcao
	x = valor
	eval(funcao)
end
puts "Digite a função: "
funcao = gets
puts "Digite a primeira aproximação: "
inicial1 = gets.to_f
puts "Digite a segunda aproximação: "
inicial2 = gets.to_f
puts "Digite o valor do erro: "
erro = gets.to_f
proximo, i, ok = 0.0, 2, true
puts "x0 = #{inicial1}"
puts "x1 = #{inicial2}"
while ok
	proximo = (inicial1*CalculaFuncao(inicial2,funcao) - inicial2*CalculaFuncao(inicial1,funcao))/(CalculaFuncao(inicial2,funcao) - CalculaFuncao(inicial1,funcao))
	if ((proximo-inicial2)/proximo).abs < erro || (CalculaFuncao(proximo,funcao)).abs < erro
		ok = false
	end
	inicial1 = inicial2
	inicial2 = proximo
	puts "x#{i} = #{proximo}"
	i+=1
end
puts "A raiz é #{proximo}"