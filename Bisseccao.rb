def CalculaFuncao valor_de_x,funcao
	x = valor_de_x
	eval (funcao)
end
puts "Digite a função: "
funcao = gets
puts "Digite o valor inicial do intervalo: "
intervalo_inferior = gets.to_f
puts "Digite o valor final do intervalo: "
intervalo_superior = gets.to_f
puts "Digite o valor do erro: "
erro = gets.to_f
ponto_medio = 0.0
while (intervalo_superior-intervalo_inferior)/2 > erro
	ponto_medio = (intervalo_inferior+intervalo_superior)/2
	if CalculaFuncao(ponto_medio,funcao) == 0
		puts "A raiz é #{ponto_medio}"
	end
	if CalculaFuncao(intervalo_inferior,funcao)*CalculaFuncao(ponto_medio,funcao) < 0
		intervalo_superior = ponto_medio
	else
		intervalo_inferior = ponto_medio
	end
end
raiz = (intervalo_inferior+intervalo_superior)/2
puts "Raiz: #{raiz}"