clear all;
close all;
%Criar Matrizes;
A = [10 2 -3 2;2 -15 3 -2;1 -3 20 2;2 2 -1 30];
R = [32; -59; -38; 160];
%Declarar valores iniciais de X
x0 = 0; w0 = 0; y0 = 0; z0 = 0;
X = [x0; w0; y0; z0];
%Verificar se converge 
B = abs(A);
%Valor do erro
e = 10^-5;
a = ((B(1,2)+B(3,2)+B(4,2))/B(2,2));
b = (((B(1,2)*a)+B(3,2)+B(4,2))/B(2,2));
c = (((B(1,3)*a)+(B(2,3)*b)+B(4,3))/B(3,3));
if ((((B(1,2)+B(1,3)+B(1,4))/B(1,1))< 1) && (((B(2,1)+B(2,3)+B(2,4))/B(2,2))<1) && (((B(3,1)+B(3,2)+B(3,4))/B(3,3))<1) && (((B(4,1)+B(4,2)+B(4,3))/B(4,4))<1))||((((B(2,1)+B(3,1)+B(4,1))/B(1,1))< 1) && (((B(1,2)+B(3,2)+B(4,2))/B(2,2))<1) && (((B(1,3)+B(2,3)+B(4,3))/B(3,3))<1) && (((B(1,4)+B(2,4)+B(3,4))/B(4,4))<1))||((((B(2,1)+B(3,1)+B(4,1))/B(1,1))< 1) && ((((B(1,2)*a)+B(3,2)+B(4,2))/B(2,2))<1) && ((((B(1,3)*a)+(B(2,3)*b)+B(4,3))/B(3,3))<1) && ((((B(1,4)*a)+(B(2,4)*b)+(B(3,4)*c))/B(4,4))<1))
    x = 0; y = 0; w = 0; z = 0; 
    n1 = 1; n2 = 1; n3 = 1; n4 = 1;
    %Deixar no Loop at� os valores estarem com o erro pedido
    while abs(n1)>e && abs(n2)>e && abs(n3)>e abs(n4)>e
       x = (R(1,1)-(A(1,2)*y0)-(A(1,3)*w0)-(A(1,4)*z0))/A(1,1);
       n1 = x-x0;
       y = (R(2,1)-(A(2,1)*x)-(A(2,3)*w0)-(A(2,4)*z0))/A(2,2);
       n2 = y-y0;
       w = (R(3,1)-(A(3,1)*x)-(A(3,2)*y)-(A(3,4)*z0))/A(3,3);
       n3 = w-w0;
       z = (R(4,1)-(A(4,1)*x)-(A(4,2)*y)-(A(4,3)*w))/A(4,4);
       n4 = z-z0;
       x0 = x; y0 = y; w0 = w; z0 = z;
    end
    %Trocar os novos valores na Matriz X
        X(1,1) = x; 
        X(2,1) = y;
        X(3,1) = w;
        X(4,1) = z;
    %Mostrar os valores de X
        X
else 'N�o Converge'
        end
   