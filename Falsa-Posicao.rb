def CalculaFuncao valor_de_x,funcao
	x = valor_de_x
	eval funcao 
end
puts "Digite a função: "
funcao = gets
puts "Digite a primeira aproximação: "
intervalo_inferior = gets.to_f
puts "Digite a segunda aproximação: "
intervalo_superior = gets.to_f
puts "Digite o valor do erro: "
erro = gets.to_f
c = 0.0
while true
	c = (intervalo_superior*CalculaFuncao(intervalo_inferior,funcao) - intervalo_inferior*CalculaFuncao(intervalo_superior,funcao)/CalculaFuncao(intervalo_inferior,funcao))
	puts "f(#{intervalo_inferior}) = #{CalculaFuncao(intervalo_inferior,funcao)}"
	puts "f(#{intervalo_superior}) = #{CalculaFuncao(intervalo_superior,funcao)}"
	if CalculaFuncao(c,funcao) == 0
		break
	end
	if CalculaFuncao(intervalo_inferior,funcao)*CalculaFuncao(intervalo_superior,funcao) < 0
		intervalo_superior = c
	else
		intervalo_inferior = c
	end
	if ((c-intervalo_inferior)/c).abs < erro || ((c-intervalo_superior)/c).abs < erro
		raiz = c
		break
	end
end
puts "Raiz: #{raiz}"
puts "f(#{raiz}) = #{CalculaFuncao(raiz,funcao)}"