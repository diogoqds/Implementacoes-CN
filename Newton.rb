def CalculaFuncao valor_de_x,funcao
	x = valor_de_x
	eval funcao
end
def DerivadaFuncao valor_de_x,derivada
	x = valor_de_x
	eval derivada
end
puts "Digite a função: "
funcao = gets
puts "Digite a derivada da função: "
derivada = gets
puts "Digite a aproximação inicial: "
inicial = gets.to_f
puts "Digite o valor do erro: "
erro = gets.to_f
i, proximo = 1, 0
puts "x0 = #{inicial}"
while true
	proximo = inicial - CalculaFuncao(inicial,funcao)/DerivadaFuncao(inicial,derivada)
	if ((proximo-inicial)/proximo).abs < erro || (CalculaFuncao(proximo,funcao)).abs < erro
		break
	end
	inicial = proximo
	puts "x#{i} = #{proximo}"
	i+=1
end
puts "Raiz: #{proximo}"