def f(valor, funcao)
	x = valor
	eval(funcao)
end
def integral(h,n,valores)
	pares, impares = [], []
	valores[1...n].each_with_index do |valor, indice|
		pares << valor if (indice+1).even?
		impares << valor if (indice+1).odd?	
	end
	soma_pares, soma_impares = 0, 0
	pares.each do |elemento|
		soma_pares += elemento
	end
	impares.each do |elemento|
		soma_impares += elemento
	end
	((h/3)*(valores[0]+valores[n]+4*soma_impares + 2*soma_pares))
end
puts "Digite a função: "
funcao = gets
puts "Digite o valor inicial do intervalo:"
a = gets.to_f
puts "Digite o valor final do intervalo:"
b = gets.to_f
puts "Qual o número de pontos: "
n = gets.to_i
h = ((b-a)/n).to_f
valores = []
for i in 0..n
	valores << f((a+i*h),funcao)
end
puts "\n\n"
puts "A integral de #{funcao} de #{a} até #{b} é #{integral(h,n,valores)}"