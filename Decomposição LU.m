clear all;
close all;
%Criar Matriz;
A = [4 1 1;1 3 -1; 1 -1 2];
A2= A;
%Verificar se a matriz 
if det(A2(1,1))>0
    if det(A2(1:2, 1:2))>0
        %Zerar o Elemento 2,1
        M21 = A2(2,1)/A2(1,1);
        A2(2, :) = A2(2, :) - (A2(1, :).*M21);
        %Zerar o Elemento 3,1
        M31 = A2(3,1)/A2(1,1);
        A2(3, :) = A2(3, :) - (A2(1, :).*M31);
        %Zerar o Elemento 3,2
        M32 = A2(3,2)/A2(2,2);
        A2(3, :) = A2(3, :) - (A2(2, :).*M32);
        %Criar matiz identidade;
        L = eye(3,3);
        L(2,1) = M21;
        L(3,1) = M31;
        L(3,2) = M32;
        %Criar Matriz U;
        U = A2;
        %Mostrar Decmposição LU
        A
        L
        U
    else 'Matriz Invalida'
    else 'Matriz Invalida'
end