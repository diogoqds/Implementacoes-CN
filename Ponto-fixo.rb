def CalculaFuncao valor,funcao
	x = valor
	eval funcao
end
puts "Digite a função de iteração: "
funcao = gets
puts "Digite a aproximação inicial: "
inicial = gets.to_f
puts "Digite o valor do erro: "
erro = gets.to_f
proximo,i =  0.0,1
puts "x0 = #{inicial}"
while true
	proximo = CalculaFuncao(inicial,funcao)
	if ((proximo-inicial)/proximo).abs < erro
		break
	end
	inicial = proximo
	puts "x#{i} = #{proximo}"
	i+=1
end
puts "Raiz: #{proximo}"