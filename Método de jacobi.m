clear all;
close all;
%Criar Matrizes;
A = [10 2 1;1 5 1;2 3 10];
R = [7; -8; 6];
%Declarar valores iniciais de X
x0 = 0.7; y0 = -1.6; z0 = 0.6;
X = [x0; y0; z0];
%Verificar se converge 
B = abs(A);
%Valor do erro
e = 10^-2;
if ((((B(1,2)+B(1,3))/B(1,1))< 1) && (((B(2,1)+B(2,3))/B(2,2))<1) && (((B(3,1)+B(3,2))/B(3,3))<1))||((((B(2,1)+B(3,1))/B(1,1))< 1) && (((B(2,1)+B(3,1))/B(1,1))< 1) && (((B(2,1)+B(3,1))/B(1,1))< 1))
    n1 = 1; n2 = 1; n3 = 1;
    %Deixar no Loop at� os valores estarem com o erro pedido
    while abs(n1)>e && abs(n2)>e && abs(n3)>e
       x = (R(1,1)-(A(1,2)*y0)-(A(1,3)*z0))/A(1,1);
       n1 = x-x0;
       y = (R(2,1)-(A(2,1)*x0)-(A(2,3)*z0))/A(2,2);
       n2 = y-y0;
       z = (R(3,1)-(A(3,1)*x0)-(A(3,2)*y0))/A(3,3);
       n3 = z-z0;
       x0 = x; y0 = y; z0 = z;
    end
    %Trocar os novos valores na Matriz X
        X(1,1) = x; 
        X(2,1) = y;
        X(3,1) = z;
    %Mostrar os valores de X
        X
else 'N�o Converge'
        end
   