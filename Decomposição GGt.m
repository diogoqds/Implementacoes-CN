clear all;
close all;
%Criar Matriz;
A = [4 2 -4;2 10 4;-4 4 9];
A2= A;
G = zeros(3,3);
%Verificar se a matriz satisfaz a condi��o
if det(A2(1,1))>0
    if det(A2(1:2, 1:2))>0
        %1� Coluna
        G(1,1) = sqrt(A2(1,1));
        G(2,1) = A2(2,1)/G(1,1);
        G(3,1) = A2(3,1)/G(1,1);
        %2� Coluna
        G(2,2) = sqrt(A2(2,2)-(G(2,1)^2));
        G(3,2) = (A2(3,2) - (G(3,1)*G(2,1)))/G(2,2);
        %3� Coluna
        G(3,3) = sqrt(A2(3,3)-(G(3,1)^2)-(G(3,2)^2));
        %Fazer a Transposta de G;
        Gt = G;
        Gt(2,1) = G(1,2);
        Gt(1,2) = G(2,1);
        Gt(3,1) = G(1,3);
        Gt(1,3) = G(3,1);
        Gt(3,2) = G(2,3);
        Gt(2,3) = G(3,2);
        %Mostrar Matrizes
        A
        G
        Gt
    else 'Matriz n�o satisfaz a condi��o'
    end
else 'Matriz n�o satisfaz a condi��o'
end